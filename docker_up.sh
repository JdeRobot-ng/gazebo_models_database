#!/bin/sh
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>

if docker ps -a | grep -q 'nginx-gazebo_models'
then
	echo 'pruning previous container'
	docker stop nginx-gazebo_models
	docker rm nginx-gazebo_models
fi

docker run --name nginx-gazebo_models \
	-v $(pwd)/build/models:/usr/share/nginx/html/gazebo_models:ro \
	-p 64730:80 \
	-d nginx


#	-v $(pwd)/models:/usr/share/nginx/html/gazebo_models:ro \
#	-v $(pwd)/nginx_location.conf:/etc/nginx/conf.d/nginx_location.conf:ro \
#	-v $(pwd)/default.conf:/etc/nginx/conf.d/default.conf:ro \

